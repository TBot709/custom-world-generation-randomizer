# Custom World Generation Randomizer

Generates randomized JSON files to import as custom world settings in Minecraft. Currently only tested on 1.16.1.

<https://tbot709.gitlab.io/custom-world-generation-randomizer>