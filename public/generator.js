var newSeed = () => {
  var seedInput = document.querySelector("#seed-input").value = Math.floor(Math.random()*Number.MAX_SAFE_INTEGER);
}

var lastSeedGenerated = "";

var outputAsLastGenerated = "";
var isEdited = () => {
  return document.querySelector("#generator-output").value !== outputAsLastGenerated;
}

var download = () => {
  var downloadLink = document.querySelector("#download-link");
  downloadLink.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(document.querySelector("#generator-output").value));
  downloadLink.setAttribute('download', "random-world-gen_1.16.1_" + lastSeedGenerated + (isEdited() ? "_edited" : "") + ".json");
  downloadLink.click();
}

var generate = () => {
  document.querySelector("#generator-output").setAttribute("style", "color: grey;");
  setTimeout(() => {
    document.querySelector("#generator-output").setAttribute("style", "color: black;");
  }, 80);

  var MAX_INT = 256;
  var MIN_INT = -1*MAX_INT;
  var MAX_DOUBLE = 1000.0;
  var MIN_DOUBLE = 0.001;

  var seed = document.querySelector("#seed-input").value;
  lastSeedGenerated = seed;
  var rng = TBot.RandomNumberGenerator.new(seed);

  var double = (min, max) => {
    return min + rng.generate()*(max - min)
  };
  var integer = (min, max) => {
    return Math.floor(double(min,max + 1)); 
  };
  var fromChoices = (choices) => {
    return choices[integer(0,choices.length - 1)]
  };
  var boolean = () => {
    return fromChoices(["true","false"]);
  };

  var bedrockRoof = rng.generate() > 0.05 ? false : true;
  var bedrockRoofPosition = bedrockRoof ? 0 : -10;
  var maxHeight = bedrockRoof ? 256 : 236;

  var bedrockFloor = rng.generate() > 0.05 ? true : false;
  var bedRockFloorPosition = bedrockFloor ? 0 : -10;

  var lavaOcean = rng.generate() > 0.2 ? false : true;
  var defaultFluidName = bedrockFloor ? (lavaOcean ? "minecraft:lava" : "minecraft:water") : "minecraft:air";

  var bottomCut = rng.generate() > 0.5 ? false : true;
  var bottomSlideTarget = bottomCut ? integer(-100, 0) : integer(0, 100);
  var TopSlideTarget = bottomCut ? integer(-100, 100) : integer(-100, 0); // if there's no bottom cut, make sure there's a top cut

  outputAsLastGenerated = document.querySelector("#generator-output").value = 
`{
  "generate_features": true,
  "bonus_chest": false,
  "seed":` + seed + `,
  "dimensions": {
    "minecraft:overworld": {
      "generator": {
        "type": "minecraft:noise",
        "seed":` + seed + `,
        "biome_source": {
          "type":"minecraft:vanilla_layered",
          "seed":` + seed + `,
          "large_biomes":` + boolean() + `
        },
        "settings": {
          "bedrock_roof_position":` + bedrockRoofPosition + `,
          "bedrock_floor_position":` + bedRockFloorPosition + `,
          "sea_level":` + integer(8,96) + `,
          "disable_mob_generation": false,
          "default_block": {
            "Name": "minecraft:stone",
            "Properties": {}
          },
          "default_fluid": {
            "Name":"` + defaultFluidName + `",
            "Properties": {
              "level": "0"
            }
          },
          "noise": {
            "density_factor":` + double(0.5,2) + `,
            "density_offset":` + double(-1,1) + `,
            "simplex_surface_noise":` + boolean() + `,
            "random_density_offset":` + boolean() + `,
            "island_noise_override":` + boolean() + `,
            "amplified":` + boolean() + `,
            "size_horizontal":` + integer(1,2) + `,
            "size_vertical":` + integer(1,2) + `,
            "height":` + maxHeight + `,
            "sampling": {
              "xz_scale":` + double(MIN_DOUBLE,3) + `,
              "y_scale":` + double(MIN_DOUBLE,3) + `,
              "xz_factor":` + double(MIN_DOUBLE,160) + `,
              "y_factor":` + double(MIN_DOUBLE,160) + `
            },
            "bottom_slide": {
              "target":` + bottomSlideTarget + `,
              "size":` + integer(0,64) + `,
              "offset": 0
            },
            "top_slide": {
              "target":` + integer(-100, 100) + `,
              "size": ` + integer(0,64) + `,
              "offset": 0
            }
          },
          "structures": {
            "stronghold": {
              "distance": 32,
              "count": 128,
              "spread": 3
            },
            "structures": {
              "minecraft:village": {
                "spacing":` + integer(9,32) + `,
                "separation": 8,
                "salt": 10387312
              },
              "minecraft:desert_pyramid": {
                "spacing":` + integer(9,32) + `,
                "separation": 8,
                "salt": 14357617
              },
              "minecraft:igloo": {
                "spacing":` + integer(9,32) + `,
                "separation": 8,
                "salt": 14357618
              },
              "minecraft:jungle_pyramid": {
                "spacing":` + integer(9,32) + `,
                "separation": 8,
                "salt": 14357619
              },
              "minecraft:swamp_hut": {
                "spacing":` + integer(9,32) + `,
                "separation": 8,
                "salt": 14357620
              },
              "minecraft:pillager_outpost": {
                "spacing":` + integer(9,32) + `,
                "separation": 8,
                "salt": 165745296
              },
              "minecraft:stronghold": {
                "spacing": 1,
                "separation": 0,
                "salt": 0
              },
              "minecraft:monument": {
                "spacing":` + integer(9,32) + `,
                "separation": 5,
                "salt": 10387313
              },
              "minecraft:endcity": {
                "spacing":` + integer(12,20) + `,
                "separation": 11,
                "salt": 10387313
              },
              "minecraft:mansion": {
                "spacing":` + integer(21,80) + `,
                "separation": 20,
                "salt": 10387319
              },
              "minecraft:buried_treasure": {
                "spacing": 1,
                "separation": 0,
                "salt": 0
              },
              "minecraft:mineshaft": {
                "spacing": 1,
                "separation": 0,
                "salt": 0
              },
              "minecraft:ruined_portal": {
                "spacing":` + integer(16,40) + `,
                "separation": 15,
                "salt": 34222645
              },
              "minecraft:shipwreck": {
                "spacing":` + integer(5,24) + `,
                "separation": 4,
                "salt": 165745295
              },
              "minecraft:ocean_ruin": {
                "spacing":` + integer(9,20) + `,
                "separation": 8,
                "salt": 14357621
              },
              "minecraft:bastion_remnant": {
                "spacing":` + integer(5,27) + `,
                "separation": 4,
                "salt": 30084232
              },
              "minecraft:fortress": {
                "spacing":` + integer(5,27) + `,
                "separation": 4,
                "salt": 30084232
              },
              "minecraft:nether_fossil": {
                "spacing": 2,
                "separation": 1,
                "salt": 14357921
              }
            }
          }
        }
      },
      "type": "minecraft:overworld"
    },
    "minecraft:the_nether": {
      "generator": {
        "type": "minecraft:noise",
        "seed":` + seed + `,
        "biome_source": {
          "type": "minecraft:multi_noise",
          "seed":` + seed + `,
          "preset": "minecraft:nether"
        },
        "settings": "minecraft:nether"
      },
      "type": "minecraft:the_nether"
    },
    "minecraft:the_end": {
      "generator": {
        "type": "minecraft:noise",
        "seed":` + seed + `,
        "biome_source": {
          "type": "minecraft:the_end",
          "seed":` + seed + `
        },
        "settings": "minecraft:end"
      },
      "type": "minecraft:the_end"
    }
  }
}`;
}