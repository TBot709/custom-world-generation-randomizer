if("undefined"===typeof TBot)var TBot={};
/*
  TBot.RandomNumberGenerator

  Usage:
  var rng = TBot.RandomNumberGenerator.new("seed");
  var randomNumber = rng.generate();
*/
TBot.RandomNumberGenerator={"new":function(b){if("undefined"===typeof b)throw new Exception("TBot.RandomNumberGenerator(seed): seed is undefined");var c={};c.seed=""+b;var a=TBot.RandomNumberGenerator.dogs(b);b=a();var d=a(),e=a();a=a();c.generate=TBot.RandomNumberGenerator.cats(b,d,e,a);return c},dogs:function(b){for(var c=0,a=2166136261;c<b.length;c++)a=Math.imul(a^b.charCodeAt(c),16777619);return function(){a+=a<<13;a^=a>>>7;a+=a<<3;a^=a>>>17;return(a+=a<<5)>>>0}},cats:function(b,
c,a,d){return function(){var e=c<<9,f=5*b;a^=b;d^=c;c^=a;b^=d;a^=e;d=d<<11|d>>>21;return(9*(f<<7|f>>>25)>>>0)/4294967296}}};